Description
===================

Find d3.ru users with the same behavior

Requirements
=================

* python 2.7.5
* python modules from requirements.txt
* redis
* mongo

Using
==========

1. Create config file. Use conf/example.yml as template
1. Run first action
  ```
  python app.py -c ../conf/example.yml -a collect
  ```
1. Check results (no failed queries and so on)
1. Run second action
  ```
  python app.py -c ../conf/example.yml -a compare
  ```
1. Find users with botlikeness > 0
  ```
  db.getCollection('karma_villains').find({'botlikeness':{'$gt':0}})
  ```

