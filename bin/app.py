#!/usr/bin/env python
from config import Config
import logging
import time
import progressbar
import redis

class Application (object):
  """Main Application Class"""

  def __init__ (self, configfile):
    self.config = Config (configfile)
    self.workers = []

  def run (self, action, suffix):
    logging.info("Application started with action=%s and suffix=%s" % (action, suffix))
    if action == 'collect':
      self.runCollect()

    elif action == 'compare':
      self.runCompare (suffix)

  def runCollect (self):
    from collect import Collect
    # fill redis 'todo' queue with usernames
    queueSize = Collect.fillTodoQueue (self.config.redis)
    # start workers
    for i in range(self.config.collect['workers']):
      self.workers.append (Collect (self.config))

    for worker in self.workers:
      worker.start()
    logging.debug ("All workers started")
    # display progress bar
    todo = queueSize
    logging.debug ("Todo queue size is %d" % queueSize)
    bar = progressbar.ProgressBar (max_value = queueSize)
    while todo > 0:
      time.sleep(1)
      todo = Collect.getTodoSize(self.config.redis, prefix = 'collect')
      bar.update (queueSize - todo)

  def runCompare (self, suffix):
    from compare import Compare
    queueSize = Compare.fillTodoQueue (self.config.redis, self.config.mongo, suffix = suffix)
    for i in range(self.config.compare['workers']):
      self.workers.append (Compare (self.config, suffix))

    for worker in self.workers:
      worker.start()
    logging.debug ("All workers started")
    # display progress bar
    todo = queueSize
    bar = progressbar.ProgressBar (max_value = queueSize)
    while todo > 0:
      time.sleep(1)
      todo = Compare.getTodoSize(self.config.redis, prefix = 'compare')
      bar.update (queueSize - todo)


#----------------------------

if __name__ == '__main__':
  import argparse
  import sys

  # parse arguments from command line
  parser = argparse.ArgumentParser (description = 'Collect, sort and compare users voting info from dirty.ru server')
  parser.add_argument ('-c', '--conf', '--config',
                       dest = 'config_path',
                       metavar = 'FILENAME',
                       help = 'Path to the yml config file',
                       required = True)
  parser.add_argument ('-a', '--action',
                       choices = ['collect', 'compare'],
                       help = 'Action to run',
                       required = True)
  parser.add_argument ('--plus',
                       action = 'store_const', const = 'heroes', dest = 'suffix', default = 'villains',
                       help = "Whether to compare pros votes instead of cons")

  args = parser.parse_args()

  # start with logging  to stdout
  logging.basicConfig (stream = sys.stdout,
                       level = logging.DEBUG)

  # Create new application instance
  app = Application (args.config_path)

  # set logging options from config
  try:
    should_reconfig_log = False
    logfile = app.config.log['file']
    f = open (logfile, 'a')
    f.close()
  except IOError as details:
    logging.warning ("Cannot open log file, %s" % details)
  except KeyError:
    logging.warning ("Log filename is not specified, proceed with stdout")
  else:
    logging.info ("File %s opened for the logging" % logfile)
    should_reconfig_log = True

  try:
    loglevel = getattr(logging, app.config.log['level'].upper())
  except AttributeError:
    logging.warning ("Wrong loglevel, using 'warning' instead")
    loglevel = 30

  if should_reconfig_log:
    for handler in logging.root.handlers[:]:
      logging.root.removeHandler(handler)
    logging.basicConfig (filename = logfile,
                         format='%(asctime)s [%(process)d] %(levelname)s: %(message)s')

  logging.getLogger().setLevel (loglevel)

  logging.info("Open log for writing")

  app.run(args.action, args.suffix)
