from worker import Worker
import logging
import pymongo

class Collect (Worker):
  """Aquire info from dirty.ru and store it into mongodb"""

  def __init__ (self, appconfig):
    Worker.__init__ (self, appconfig.redis, prefix = 'collect')
    # Should not connect to mongo before forking.
    # http://api.mongodb.org/python/current/faq.html#using-pymongo-with-multiprocessing
    self.mongodb = pymongo.MongoClient (appconfig.mongo['host'],
                                        appconfig.mongo['port'],
                                        connect = False)[appconfig.mongo['db']]
    self.url = "https://dirty.ru/ajax/user/karma/list/"

  def userRequest(self, userid):
    import time
    import requests

    # first get total voters
    data = {'user': userid, 'limit':0}
    r = requests.post (self.url, data)
    try:
      result = r.json()
    except ValueError:
      logging.warning ("Cannot parse non-json response: %s" % r.text)
      return False

    if result['status'] != 'OK':
      logging.info("Cannot find user %s" % userid)
      return False

    # get all votes
    data['limit'] = max (result['cons_count'], result['pros_count'])
    r = requests.post (self.url, data)
    try:
      result = r.json()
    except ValueError:
      logging.warning ("Cannot parse non-json response %s" % r.text)
      return False
    if result['status'] != 'OK':
      logging.info("Cannot get votes to user %s" % userid)
      return False
    pros_voters = result['pros'] if result['pros'] else []
    cons_voters = result['cons'] if result['cons'] else []

    victim = {'userid': userid, 'villains': [], 'heroes': []}
    for voter in pros_voters:
#      logging.debug ("+ %s" % voter)
      victim['heroes'].append ({'userid': voter['user']['id'], 'name': voter['user']['login']})

    for voter in cons_voters:
#      logging.debug ("- %s" % voter)
      victim['villains'].append ({'userid': voter['user']['id'], 'name': voter['user']['login']})

    return victim

  def mainLoop (self):
    # pop job from redis 'todo' queue and add it to 'processing' queue
    userid = self.getNewJob()

    if userid:
      logging.debug ("Processing %s" % userid)
    else:
      logging.info ("Todo queue is empty, exiting")
      return False

    victim = self.userRequest (userid)
    # {'userid':'userid1', 'villains':[user2, user3], 'heroes':[user4, user5]}
    if not victim:
      self.failCurrentJob()
      return True # go for next user

    self.mongodb.karma_victims.insert_one(victim)

    for villain in victim['villains']:
      self.mongodb.karma_villains.update_one({'userid': villain['userid'],
                                              'name': villain['name']},
                                             {'$push': {'victims':victim['userid']}},
                                             upsert = True)
    for hero in victim['heroes']:
      self.mongodb.karma_heroes.update_one({'userid': hero['userid'],
                                              'name': hero['name']},
                                             {'$push': {'victims':victim['userid']}},
                                             upsert = True)

    self.succeedCurrentJob()
    logging.debug ("%s done" % userid)
    return True

  def run(self):
    logging.info ("Collect worker started")
    result = True
    while result:
      result = self.mainLoop()
      # result == False means exit the loop

  @staticmethod
  def fillTodoQueue(redisconfig, prefix = 'collect'):
    """Initial fill redis 'todo' queue with usernames
       or exit if queue is not empty
       Return current todo queue size"""
    import redis

    r =  redis.StrictRedis (host = redisconfig['host'],
                            port = redisconfig['port'],
                            db = redisconfig['db'])
    if r.llen('%s_todo' % prefix) > 0:
      logging.info("Todo queue is not empty")
      return r.llen('%s_todo' % prefix)

    #TODO: create actual filling function instead of dummy
    logging.info("Filling todo queue")
    f = open ("../test.txt", 'r')
    for line in f.readlines():
      r.rpush('%s_todo' % prefix, int(line))

    return r.llen('%s_todo' % prefix)

