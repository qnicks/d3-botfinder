import yaml
import logging

class ConfigError (Exception):
  def __init__ (self, value):
    self.value = value

  def __str__ (self):
    return repr (self.value)

class Config (object):
  """Class to store all configuration options"""

  __defaults = {'general':
                {'api_url': 'http://dirty.ru/api/'},
               'collect':
                {'workers': None}, # None == cpu_number
               'compare':
                {'workers': None},
              'log':
               {'level': 'warning'}
             }

  def __init__ (self, filename):
    try:
      data = yaml.safe_load (file(filename,'r'))
    except IOError as detail:
      raise ConfigError(detail)

    # logs config
    try:
      self.log = data['log']
    except KeyError:
      logging.warning("Cannot find logging options, proceed with stdout")
      self.log = type(self).__defaults['log']

    #redis config
    try:
      self.redis = data['redis']
    except KeyError:
      raise ConfigError("Cannot find redis client options, exiting")

    #mongo config
    try:
      self.mongo = data['mongo']
    except KeyError:
      raise ConfigError("Cannot find mongo client options, exiting")

    # general config
    try:
      self.general = data['general']
    except KeyError:
      logging.warning("Cannot find general options, proceed with default")
      self.general = type(self).__defaults['general']

    try:
      self.collect = data['collect']
    except KeyError:
      logging.warning("Cannot find collect options, proceed with default")
      self.collect = type(self).__defaults['collect']

    try:
      self.compare = data['compare']
    except KeyError:
      logging.warning("Cannot find compare options, proceed with default")
      self.compare = type(self).__defaults['compare']
