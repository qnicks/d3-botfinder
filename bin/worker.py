from multiprocessing import Process
import logging
import redis

class Worker (Process):
  """Worker pops job from 'todo' redis queue,
     push it to 'processing' queue while procssing
     and then push it to 'success' or 'failed' queues"""

  def __init__ (self, redisconfig, prefix = ''):
    """use different prefix for different pools of workers"""
    Process.__init__(self)

    self.redis = redis.StrictRedis (host = redisconfig['host'],
                                    port = redisconfig['port'],
                                    db = redisconfig['db'])

    self.prefix = prefix
    self.currentJob = None
    logging.debug ("Init worker")

  def getNewJob (self):
    self.currentJob = self.redis.rpoplpush ('%s_todo' % self.prefix, '%s_processing' % self.prefix)
    return self.currentJob

  def failCurrentJob (self):
    self.redis.lrem ('%s_processing' % self.prefix, 1, self.currentJob)
    self.redis.lpush ('%s_failed' % self.prefix, self.currentJob)
    self.currentJob = None

  def succeedCurrentJob (self):
    self.redis.lrem ('%s_processing' % self.prefix, 1, self.currentJob)
    self.redis.lpush ('%s_success' % self.prefix, self.currentJob)
    self.currentJob = None

  @staticmethod
  def getTodoSize(redisconfig, prefix):
    """Retutn current size of todo queue"""

    r =  redis.StrictRedis (host = redisconfig['host'],
                            port = redisconfig['port'],
                            db = redisconfig['db'])
    return r.llen('%s_todo' % prefix)

