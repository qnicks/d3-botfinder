from worker import Worker
import logging
import pymongo

class Compare (Worker):
  """Compare villains' victims list with each other
     and find similar ones"""

  def __init__ (self, appconfig, suffix = 'villains'):
    Worker.__init__ (self, appconfig.redis, prefix = 'compare')
    # Should not connect to mongo before forking.
    # http://api.mongodb.org/python/current/faq.html#using-pymongo-with-multiprocessing
    self.mongodb = pymongo.MongoClient (appconfig.mongo['host'],
                                        appconfig.mongo['port'],
                                        connect = False)[appconfig.mongo['db']]
    self.suffix = suffix

  def compareToEveryone(self, suspected):
    f = {'100%':[], '95%':[], '90%':[], '80%':[], '70%':[]}
    if suspected['victims'].__len__() < 2:
      return f
    for user in self.mongodb['karma_%s' % self.suffix].find():
      # do not compare suspected with themselves
#      if user['userid'] == suspected['userid']:
#        continue
      # go to next user if user['victims'] is less than 2
#      if user['victims'].__len__() < 2:
#        continue

      commonSize = list (set (suspected['victims']) & set(user['victims'])).__len__()
      differenceSize = list (set (suspected['victims']) ^ set(user['victims'])).__len__()
      unionSize = commonSize + differenceSize
      likeness = 100 * commonSize / unionSize
      if likeness == 100:
        f['100%'].append (user['userid'])
      elif likeness >= 95:
        f['95%'].append (user['userid'])
      elif likeness >= 90:
        f['90%'].append (user['userid'])

    return f

  def calculateBotLikeness (self,f):
    b = 1*f['100%'].__len__() + \
        0.95 * f['95%'].__len__() + \
        0.9 * f['90%'].__len__()
    return b

  def mainLoop (self):
    userid = self.getNewJob()

    if userid:
      logging.debug ("Processing %s" % userid)
    else:
      logging.info ("Todo queue is empty, exiting")
      return False

    try:
      suspected = self.mongodb['karma_%s' % self.suffix].find_one ({'userid': int(userid)})
      logging.debug ('karma_%s' % self.suffix)
    except Exception as details:
      self.failCurrentJob()
      logging.error ("Cannot process user %s, %s, exiting" % (userid, details))
      return False

    # f == {'100%':['username1','username2'], '95%':['username3'], etc...}
    f = self.compareToEveryone (suspected)
    b = self.calculateBotLikeness (f)

    # save botolikeness and p as 'friends' to mongo
    suspected['friends'] = f
    suspected['botlikeness'] = b
    try:
      self.mongodb['karma_%s' % self.suffix].replace_one ({'_id':suspected['_id']}, suspected)
    except Exception as details:
      self.failCurrentJob()
      logging.error ("Cannot process user %s, %s, exiting" % (userid, details))
      return False

    self.succeedCurrentJob()
    logging.debug ("%s done" % userid)
    return True

  def run (self):
    logging.info ("Compare worker started")
    result = True
    while result:
      result = self.mainLoop()

  @staticmethod
  def fillTodoQueue(redisconfig, mongoconfig, prefix = 'compare', suffix = 'villains'):
    """Initial fill redis 'todo' queue with usernames
       or exit if queue is not empty
       Return current todo queue size"""
    import redis

    r =  redis.StrictRedis (host = redisconfig['host'],
                            port = redisconfig['port'],
                            db = redisconfig['db'])
    if r.llen('%s_todo' % prefix) > 0:
      logging.info("Todo queue is not empty")
      return r.llen('%s_todo' % prefix)

    mongodb = pymongo.MongoClient (mongoconfig['host'],
                                   mongoconfig['port'])[mongoconfig['db']]
    for user in mongodb['karma_%s' % suffix].find():
      r.lpush('%s_todo' % prefix, int(user['userid']))

    return r.llen('%s_todo' % prefix)
